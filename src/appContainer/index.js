import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as AppActions from './actions';
import { Header } from "../components/header";
import { Navbar } from "../components/navbar";
import { Home } from "../components/home";
import { Routes } from "../routes";

class App extends Component {

  render() {
    const { postsData, todosData, selectedTodo, selectedPost } = this.props;

    return (
      <main>
        <Header/>
        <Navbar/>
        <Home/>
      </main>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    postsData: state.appReducers.postsData,
    todosData: state.appReducers.todosData,
    selectedTodo: state.appReducers.selectedTodo,
    selectedPost: state.appReducers.selectedPost
  }
};

export default connect(mapStateToProps, AppActions)(App);