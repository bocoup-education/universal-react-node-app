import { RECEIVE_POSTS_DATA, SELECT_POST } from "./actions";

const initialState = {
  postsData: null,
  todosData: null,
  selectedPost: null,
  selectedTodo: null
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case RECEIVE_POSTS_DATA:
      return Object.assign({}, state, {
        postsData: action.payload
      });

    default:
      return state;
  }
}