import React from 'react';
import { Home } from "../components/home";
import { Posts } from "../components/posts";
import App from '../appContainer';
import { StaticRouter, BrowserRouter, Route } from 'react-router-dom'

const Routes = () => {
  return (
    <div>
      <Route path="/" component={App}>
        <Route path="/posts" componet={Posts}/>
      </Route>
    </div>
  )
}

export const BrowserRoutes = () => {
  return (
    <BrowserRouter>
      <Routes/>
    </BrowserRouter>
  )
}

export const ServerRoutes = ({path}) => {
  return (
    <StaticRouter location={path} context={{}}>
      <Routes/>
    </StaticRouter>
  )
}