import React from 'react';
import './style.css';
import { Link } from 'react-router-dom';

export const Navbar = () => {
  return (
    <nav>
      <ul>
        <li>Home</li>
        <li><Link to="/posts">Posts</Link></li>
        <li><Link to="/todos">Todos</Link></li>
      </ul>
    </nav>
  )
};