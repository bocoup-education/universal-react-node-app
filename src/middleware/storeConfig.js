import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import rootReducer from './rootReducer';
import promiseMiddleware from 'redux-promise';
import thunk from 'redux-thunk';

export const logger = createLogger();

const middlewareConfig =  applyMiddleware(
  logger, // redux state logger TODO only add this when in development mode
  promiseMiddleware, // redux promise action resolver
  thunk // redux thunk for async actions
);

export const storeConfig = (initialState) => {
  let state = initialState;

  if(typeof state === 'string') {
    state = JSON.parse(initialState);
  }

  return createStore(rootReducer, state, middlewareConfig);
}