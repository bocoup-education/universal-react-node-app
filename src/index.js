import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { storeConfig } from './middleware/storeConfig';
import { BrowserRoutes } from "./routes";

const initialState = {};

const store = storeConfig(initialState);

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRoutes/>
  </Provider>,
document.getElementById('universal-node-summit-app'));
