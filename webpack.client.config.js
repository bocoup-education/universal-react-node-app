const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  target: 'web',
  entry: [ "babel-polyfill", './src/index.js' ],
  mode: process.env.NODE_ENV,
  devtool: 'inline-source-map',
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ],
  resolve: {},
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
       test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: path.resolve(__dirname,  'public'),
            }
          },
             'css-loader'
           ]
      },
      {
       test: /\.(png|svg|jpg|jpeg|gif)$/,
        use: [
             'file-loader'
           ]
       }
    ]
  },
  output: {
    filename: 'client.bundle.js',
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
  }
};