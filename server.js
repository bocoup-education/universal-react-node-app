const express = require('express');
const app = express();
const { html } = require('./src/server/renderer');

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.send(html(req.path));
});

app.listen(3000, () => {
  console.log('Listening on port 3000');
});